-- DROPPING DATABASE
DROP DATABASE IF EXISTS klubaner_companies;
-- creating database
CREATE DATABASE klubaner_companies;


-- creating users and privileges

GRANT ALL ON klubaner_companies.* TO klubaner_wkelik@localhost IDENTIFIED BY 'Cahyadi_1231' WITH GRANT OPTION;
GRANT SELECT, UPDATE ON klubaner_companies.* TO klubaner_comsubs@localhost IDENTIFIED BY 'Parakanc@ngg4h' WITH GRANT OPTION;
