DROP TABLE IF EXISTS mstArea;
CREATE TABLE mstArea(
    areaId varchar(10) ,
    areaParent varchar(10)  default 0,
    areaName varchar(40),
    PRIMARY KEY (areaId)
);


DROP TABLE IF EXISTS mstLineOfBussines;
CREATE TABLE mstLineOfBussines(
    lobId int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
    lobName tinytext,
    PRIMARY KEY (lobId)
);

DROP TABLE IF EXISTS palm_comp;
CREATE TABLE IF NOT EXISTS palm_comp (
  ID int(11) NOT NULL AUTO_INCREMENT COMMENT 'Primary key, for registration purposes',
  comp_name varchar(254) NOT NULL COMMENT 'registered company name',
  lob_id int(10) UNSIGNED NOT NULL,
  country varchar(85) NOT NULL COMMENT 'registered country address of the company',
  address text NOT NULL COMMENT 'full address of the company',
  postcode int(5) DEFAULT NULL COMMENT 'registered postcode of the company',
  provinsi varchar(85) DEFAULT NULL,
  phone varchar(75) DEFAULT NULL,
  fax varchar(45) DEFAULT NULL,
  email varchar(85) DEFAULT NULL,
  web varchar(120) DEFAULT NULL,
  bizz_line varchar(85) DEFAULT NULL,
  detail text DEFAULT NULL,
  CONSTRAINT bizzclass FOREIGN KEY (lob_id) REFERENCES mstLineOfBussines(lobId) ON DELETE CASCADE ON UPDATE RESTRICT,
  PRIMARY KEY (ID)
);


DROP TABLE IF EXISTS companyAktes;
CREATE TABLE companyAktes(
   aktRecord int(6) UNSIGNED AUTO_INCREMENT,
   companyId int(11) NOT NULL,
   aktNomor varchar(50) NOT NULL,
   aktTanggal date,
   aktNotarisNama varchar(50),		
   aktNotarisKantor varchar(50),
   aktNotarisAlamatBuilding varchar(50),
   aktNotarisAlamatStreet varchar(50),
   aktNotarisAlamatDistrict varchar(50) NOT NULL,
   aktNotarisAlamatProvince varchar(50) NOT NULL,
   -- referencing company id --
   CONSTRAINT aktCompId FOREIGN KEY(companyId) REFERENCES palm_comp(ID) ON DELETE CASCADE ON UPDATE RESTRICT,
   PRIMARY KEY(aktRecord)
);



DROP TABLE IF EXISTS companyAddresses;
CREATE TABLE companyAddresses(
    addrRecord int(6) UNSIGNED AUTO_INCREMENT,
    companyId int(11) NOT NULL,
    addrBuilding varchar(50),
    addrStreet varchar(50),
    addrCity varchar(50),
    addrProvince varchar(50),
    phoneLine1 varchar(16),
    phoneLine2 varchar(16),
    phoneLine3 varchar(16),
    faxLine1 varchar(16),
    faxLine2 varchar(16),
    faxLine3 varchar(16),

    -- referencing company id --
    CONSTRAINT addrCompId FOREIGN KEY(companyId) REFERENCES palm_comp(ID) ON DELETE CASCADE ON UPDATE RESTRICT,

    PRIMARY KEY (addrRecord)
);

DROP TABLE IF EXISTS branchOffices;
CREATE TABLE branchOffices(
    branchId int(6) UNSIGNED AUTO_INCREMENT,
    companyId int(11) NOT NULL,
    branchEstablished date,
    branchClosed date,
    branchMgrName varchar(50) NOT NULL,
    branchMgrPosition varchar(50) NOT NULL,
    branchAddrBuilding varchar(50),
    branchAddrStreet varchar(50),
    branchAddrCity varchar(50),
    branchAddrProvince varchar(50),
    branchPhoneLine1 varchar(16),
    branchPhoneLine2 varchar(16),
    branchPhoneLine3 varchar(16),
    branchFaxLine1 varchar(16),
    branchFaxLine2 varchar(16),
    branchFaxLine3 varchar(16),
    branchLastUpdate timestamp DEFAULT CURRENT_TIMESTAMP(),

    -- referencing company id --
    CONSTRAINT brchCompId FOREIGN KEY(companyId) REFERENCES palm_comp(ID) ON DELETE CASCADE ON UPDATE RESTRICT,
    PRIMARY KEY (branchId)
);


DROP TABLE IF EXISTS siteplants;
CREATE TABLE siteplants(
    sitePlantId int(10) UNSIGNED AUTO_INCREMENT,
    companyId int(11) NOT NULL,
    spName tinytext,
    spEstablished date,
    spOpStarted date,
    spOpEnded date,
    spLocArea varchar(10),
    spAreaTotal float(8,2) DEFAULT 0.0,
    spAreaTanamLuas float(8,2) DEFAULT 0.0,
    spAreaTanamJenis varchar(50),
    spLastUpdate timestamp DEFAULT CURRENT_TIMESTAMP(),

    -- referencing company id --
    CONSTRAINT sitePlantComp FOREIGN KEY(companyId) REFERENCES palm_comp(ID) ON DELETE CASCADE ON UPDATE RESTRICT,

    -- referencing area id --
    CONSTRAINT sitePlantArea FOREIGN KEY(spLocArea) REFERENCES mstArea(areaId) ON DELETE CASCADE ON UPDATE RESTRICT,
    PRIMARY KEY (sitePlantId)
);

DROP TABLE IF EXISTS sitePlantProductions;
CREATE TABLE sitePlantProductions(
    spProdRecord int(10) UNSIGNED AUTO_INCREMENT,
    sitePlantId int(10) UNSIGNED,
    spCapacity float(8,2) DEFAULT 0.0,
    spCapacityUpdate timestamp DEFAULT CURRENT_TIMESTAMP(),

    -- Referencing siteplant --
    CONSTRAINT sppSiteId FOREIGN KEY (spProdRecord) REFERENCES siteplants(sitePlantId) ON DELETE CASCADE ON UPDATE RESTRICT,

    PRIMARY KEY (spProdRecord)
);

DROP TABLE IF EXISTS companyShared;
CREATE TABLE companyShared(
    shareRecord int(10) UNSIGNED AUTO_INCREMENT,
    companyId int(11) NOT NULL,
    shareholderName varchar(50),
    shareholderPercentage float(8,2) DEFAULT 0.0,
    sharedLastUpdate timestamp DEFAULT CURRENT_TIMESTAMP(),

    -- referencing company id --
    CONSTRAINT shareCompanyId FOREIGN KEY(companyId) REFERENCES palm_comp(ID) ON DELETE CASCADE ON UPDATE RESTRICT,
    PRIMARY KEY (shareRecord)
);

DROP TABLE IF EXISTS bankers;
CREATE TABLE bankers(
    bankersRecord INT(10) UNSIGNED AUTO_INCREMENT,
    companyId int(11) NOT NULL,
    bankersName varchar(50) NOT NULL,
    bankersStatus enum('main','minority') DEFAULT 'main',
    bankersLastUpdate timestamp DEFAULT CURRENT_TIMESTAMP(),
    -- referencing company id --
    CONSTRAINT bankCompanyId FOREIGN KEY(companyId) REFERENCES palm_comp(ID) ON DELETE CASCADE ON UPDATE RESTRICT,
    PRIMARY KEY (bankersRecord)
);

DROP TABLE IF EXISTS companyEmployee;
CREATE TABLE companyEmployee(
    emplRecord int(10) UNSIGNED AUTO_INCREMENT,
    companyId int(11) NOT NULL,
    empltotal int(6) DEFAULT 0,
    emplPartLocal int(6) DEFAULT 0,
    emplPartExpat int(6) DEFAULT 0,
    emplLastUpdate timestamp DEFAULT CURRENT_TIMESTAMP(),

    -- referencing company id --
    CONSTRAINT emplCompanyId FOREIGN KEY(companyId) REFERENCES palm_comp(ID) ON DELETE CASCADE ON UPDATE RESTRICT,
    PRIMARY KEY (emplRecord)
);


DROP TABLE IF EXISTS companyBoc;
CREATE TABLE companyBoc(
    bocRecord int(10) UNSIGNED AUTO_INCREMENT,
    bocCompany int(11) NOT NULL,
    bocPosition varchar(50),
    bocName varchar(50),
    bocStartInOffice date,
    bocLastUpdate timestamp DEFAULT CURRENT_TIMESTAMP(),

    -- referencing company id --
    CONSTRAINT bocCompId FOREIGN KEY(bocCompany) REFERENCES palm_comp(ID) ON DELETE CASCADE ON UPDATE RESTRICT,
    PRIMARY KEY (bocRecord)
);

DROP TABLE IF EXISTS companyBod;
CREATE TABLE companyBod(
    bodRecord int(10) UNSIGNED AUTO_INCREMENT,
    bodCompany int(11) NOT NULL,
    bodPosition varchar(50),
    bodName varchar(50),
    bodStartInOffice date,
    bodLastUpdate timestamp DEFAULT CURRENT_TIMESTAMP(),

    -- referencing company id --
    CONSTRAINT bodCompId FOREIGN KEY(bodCompany) REFERENCES palm_comp(ID) ON DELETE CASCADE ON UPDATE RESTRICT,
    PRIMARY KEY (bodRecord)
);

DROP TABLE IF EXISTS companyManagers;
CREATE TABLE companyManagers(
    mgrRecord int(10) UNSIGNED AUTO_INCREMENT,
    mgrCompany int(11) NOT NULL,
    mgrPosition varchar(50),
    mgrName varchar(50),
    mgrStartInOffice date,
    mgrLastUpdate timestamp DEFAULT CURRENT_TIMESTAMP(),

    -- referencing company id --
    CONSTRAINT mgrCompId FOREIGN KEY(mgrCompany) REFERENCES palm_comp(ID) ON DELETE CASCADE ON UPDATE RESTRICT,
    PRIMARY KEY (mgrRecord)
);

DROP TABLE IF EXISTS associates;
CREATE TABLE associates(
    assocRecord int(10) UNSIGNED AUTO_INCREMENT,
    assocCompany int(11) NOT NULL,
    assocCompanyPosition varchar(50),
    assocCompanyGroupName varchar(50),

    -- referencing company id --
    CONSTRAINT assocCompId FOREIGN KEY(assocCompany) REFERENCES palm_comp(ID) ON DELETE CASCADE ON UPDATE RESTRICT,
    PRIMARY KEY (assocRecord)
);

DROP TABLE IF EXISTS mstSubcriptions;
CREATE TABLE mstSubcriptions(
    subsId int(10) UNSIGNED AUTO_INCREMENT,
    subsName varchar(50) NOT NULL,
    -- Recheck, subs period == durasi subscription dalam bulan
    subsPeriod int(3) DEFAULT 1,
    subsAmount int(10) UNSIGNED DEFAULT 0,
    PRIMARY KEY(subsId)
);

DROP TABLE IF EXISTS subscribers;
CREATE TABLE subscribers(
    scrbrId INT(10) UNSIGNED,
    scrbrUsername varchar(30),
    scrbrPassword varchar(32),
    scrbrPayPeriod varchar(7) DEFAULT '0000-00',
    scrbrPayLast date,
    CONSTRAINT scrbrSubsId FOREIGN KEY (scrbrId) REFERENCES mstSubcriptions(subsId) ON DELETE CASCADE ON UPDATE RESTRICT
);

-- klubaner_companies klubaner_wkelik Cahyadi_1231