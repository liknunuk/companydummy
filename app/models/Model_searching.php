<?php
class Model_searching
{
    // private $table = "nama tabel";
    private $db;

    public function __construct()
    {
        $this->db = new Database();
    }

    
    public function provinces(){
        $sql = "SELECT areaId , areaName FROM mstArea WHERE areaParent = '0' ORDER BY areaId";
        $this->db->query($sql);
        return $this->db->resultSet();
    }
    
    
   
    public function combypro($provId){
        $sql = "SELECT ID , comp_name , address , areaName , lobName FROM palm_comp , mstArea , mstLineOfBussines WHERE mstArea.areaId = palm_comp.provinsi && mstLineOfBussines.lobId = palm_comp.bizz_line && provinsi=:provId";
        $this->db->query($sql);
        $this->db->bind('provId',$provId);
        return $this->db->resultSet();

    }
    
    public function compCategories(){
        $sql ="SELECT companyCategory FROM compCat GROUP BY companyCategory ORDER BY companyCategory LIMIT 100";
        $this->db->query($sql);
        return $this->db->resultSet();
    }

    public function compLoB(){
        $sql ="SELECT * FROM mstLineOfBussines ORDER BY lobName LIMIT 100";
        $this->db->query($sql);
        return $this->db->resultSet();
    }

    public function companyByCat($keyword){
        $sql ="SELECT companyId, companyName, establishedDate FROM mstCompanies WHERE companyCategory=:keyword";
        $this->db->query($sql);
        $this->db->bind('keyword',$keyword);
        return $this->db->resultSet();        
    }

    public function companyByLob($keyword){
        $sql ="SELECT companyId, companyName, establishedDate FROM mstCompanies WHERE companyLoB=:keyword";
        $this->db->query($sql);
        $this->db->bind('keyword',$keyword);
        return $this->db->resultSet();        
    }

    public function genData($id){
        $sql ="SELECT palm_comp.* , mstArea.areaName province, mstLineOfBussines.lobName FROM palm_comp, mstLineOfBussines, mstArea WHERE ID = :id && mstLineOfBussines.lobId = palm_comp.bizz_line && mstArea.areaId = palm_comp.provinsi";
        $this->db->query($sql);
        $this->db->bind('id',$id);
        return $this->db->resultOne();
    }

    public function comAddreses($id){
        $sql ="SELECT a.* , b.areaName city, c.areaName prov FROM companyAddresses a , mstArea b, mstArea c WHERE companyId = 4 && b.areaId = a.addrCity && c.areaId = a.addrProvince";
        $this->db->query($sql);
        $this->db->bind('id',$id);
        return $this->db->resultSet();
    }

    public function comAkte($id){
        $sql ="SELECT a.* , b.areaName district, c.areaName province FROM companyAktes a, mstArea b, mstArea c WHERE companyId = :id && b.areaId = a.aktNotarisAlamatDistrict && c.areaId = a.aktNotarisAlamatProvince";
        $this->db->query($sql);
        $this->db->bind('id',$id);
        return $this->db->resultSet();
    }

    public function comBranches($id){
        $sql ="SELECT a.* , b.areaName city, c.areaName province FROM branchOffices a, mstArea b, mstArea c WHERE companyId=:id && b.areaId = a.branchAddrCity && c.areaId = a.branchAddrProvince";
        $this->db->query($sql);
        $this->db->bind('id',$id);
        return $this->db->resultSet();
    }

    public function comEmployees($id){
        $sql ="SELECT emplPartLocal empLocal, emplPartExpat empExpat , emplTotal FROM companyEmployee WHERE companyId = :id";
        $this->db->query($sql);
        $this->db->bind('id',$id);
        return $this->db->resultSet();
    }

    public function comSitePlants($id){
        $sql ="SELECT sp.*, prod.spCapacity capacity , prod.spCapacityUpdate lupdate FROM siteplants sp , sitePlantProductions prod WHERE companyId = :id && prod.sitePlantId = sp.sitePlantId";
        $this->db->query($sql);
        $this->db->bind('id',$id);
        $result = $this->db->resultSet();
        $output =[];
        foreach($result AS $sp){
            $location = $this->spLocation($sp['spLocArea']);
            $output=['sp'=>$result,'loc'=>$location]; 
        }

        return $output;
    }

    private function spLocation($loc){
        $kdDesa = $loc;
        $kdKcmt = substr($loc,0,6);
        $kdKota = substr($loc,0,4);
        $kdProp = substr($loc,0,2);
        $area = "";
        $kdArea = [ $kdDesa , $kdKcmt , $kdKota , $kdProp ];
        for( $i =0 ; $i < COUNT($kdArea) ; $i++ ){
            $sql = "SELECT areaName FROM mstArea WHERE areaId =:id ";
            $this->db->query($sql);
            $this->db->bind('id',$kdArea[$i]);
            $result = $this->db->resultOne();
            $area.=$result['areaName'].", ";
        }

        return rtrim($area,",");
    }



    // companyAddresses , companyAktes , branchoffices , companyEmployee , sitePlants, sitePlantsProductions

}

/*
$sql ="";
$this->db->query($sql);
$this->db->bind('xxx',$xxx);
$this->db->execute();
return $this->db->rowCount();
return $this->db->resultOne();
return $this->db->resultSet();
*/