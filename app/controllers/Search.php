<?php
// sesuaikan nama kelas, tetap extends ke Controller
class Search extends Controller
{
  public function index(){ echo "Hai"; }

  public function cbcat($keyword)
  {
    $keyword = str_replace("-"," ",$keyword);
    // echo "Keyword=".$keyword;
    $data = $this->model('Model_searching')->companyByCat($keyword);
    echo json_encode($data,JSON_PRETTY_PRINT);
  }

  public function cblob($keyword)
  {
    $data = $this->model('Model_searching')->companyByLob($keyword);
    echo json_encode($data,JSON_PRETTY_PRINT);
  }

  public function companyInfo($id){
    
    $output = [
      'generalData' => $this->model('Model_searching')->genData($id),
      'addresses'=>$this->model('Model_searching')->comAddreses($id),
      'aktes'=>$this->model('Model_searching')->comAkte($id),
      'branches'=>$this->model('Model_searching')->comBranches($id),
      'employees'=>$this->model('Model_searching')->comEmployees($id),
      'sitePlants'=>$this->model('Model_searching')->comSitePlants($id)
    ];

    echo json_encode($output,JSON_PRETTY_PRINT);
  }

  public function combypro($areaId){
    $companies = $this->model('Model_searching')->combypro($areaId);
    echo json_encode($companies,JSON_PRETTY_PRINT);
  }
}
