<?php
// sesuaikan nama kelas, tetap extends ke Controller
class Home extends Controller
{
  // method default
  public function index()
  {
    $data['title']="Companies For Dummy";
    // $data['ctg'] = $this->model('Model_searching')->compCategories();
    // $data['lob'] = $this->model('Model_searching')->compLoB();
    $data['provinces'] = $this->model('Model_searching')->provinces();
    $this->view('template/header',$data);
    $this->view('home/index',$data);
    $this->view('template/footer');
  }

  public function companyProfile($id){
    $data['title']="Company Info";

    $data['comGdata'] = $this->model('Model_searching')->genData($id);
    $data['comAddrs'] = $this->model('Model_searching')->comAddreses($id);
    $data['comAktes'] = $this->model('Model_searching')->comAkte($id);
    $data['comBranch']= $this->model('Model_searching')->comBranches($id);
    $data['comEmploy']= $this->model('Model_searching')->comEmployees($id);
    $data['comSitePlant'] = $this->model('Model_searching')->comSitePlants($id);
    // companyAddresses , companyAktes , branchoffices , companyEmployee , sitePlants, sitePlantsProductions
    $this->view('template/header',$data);
    $this->view('home/profiles',$data);
    $this->view('template/footer');
  }

  // public function companyProfile($id){
  //   $data['title'] = "COMPANY PROFILE";
  //   $data['id'] = $id;
  //   $this->view('template/header',$data);
  //   $this->view('home/profiles',$data);
  //   $this->view('template/footer');
  // }
}
