<div class="container-fluid">
  <div class="row">
    <div class="col-lg-3">
        <p>Company by:</p>
        <select id="filter1" class="form-control">
        <option value="">Choose Filter</option>
        <option value="cat">Categories</option>
        <option value="lob">Line of Bussiness</option>
        </select>
    </div>
    
    <div class="col-lg-3">
        <p>Choises</p>
        <select name="compCat" id="filter2" class="form-control options">
            <option>Choose categories</option>
            <?php foreach($data['ctg'] as $ctg): ?>
            <option value="<?=str_replace(" ","-",$ctg['companyCategory']);?>"><?=$ctg['companyCategory'];?></option>
            <?php endforeach; ?>
        </select>
    <!-- </div>

    <div class="col-lg-3"> -->
        <!-- <p>Choises</p> -->
        <select name="compLoB" id="filter3" class="form-control options">
            <option>Choose Line of Bussines</option>
            <?php foreach($data['lob'] as $lob): ?>
            <option value="<?=$lob['lobId'];?>"><?=$lob['lobName'];?></option>
            <?php endforeach; ?>
        </select>
    </div>
    <div class="col-lg-3">
      <div id="urlSeek"></div>
    </div>

  </div>

  <div class="row">
    <div class="col-lg-12">
      <table class="table table-sm table-striped">
        <thead>
            <tr>
                <th>Company Name</th>
                <th>Established Date</th>
                <th>Details</th>
            </tr>
        </thead>
        <tbody id="searchResult"></tbody>
      </table>
    </div>
  </div>
</div>

<?php $this->view('template/bs4js'); ?>
<script>
$(document).ready(function(){
    let tabkey="", keyword="";
    $("#filter1").change(function(){
        if( this.value == 'cat' ){
            $(".options").hide();
            $("#filter2").show();
            tabkey="compCat";
        }else{
            $(".options").hide();
            $("#filter3").show();
            tabkey="compLoB"
        }
    });

    $("#filter2").change(function(){
        let urlSeek = "<a href='<?=BASEURL;?>Search/cbcat/"+this.value+"'>Link Json</a>";
        $('#urlSeek').html('');
        $('#urlSeek').html(urlSeek);
        $.ajax({
            dataType:'json',
            url: '<?=BASEURL;?>Search/cbcat/'+this.value,
            success: function(resp){
                $("#searchResult tr").remove();
                $.each(resp,function(i,data){
                    $("#searchResult").append(`
                    <tr>
                    <td>${data.companyName}</td>
                    <td>${data.establishedDate}</td>
                    <td>
                        <a href="<?=BASEURL;?>Search/companyInfo/${data.companyId}">
                            Detail
                        </a>
                    </td>
                    </tr>
                    `);
                })
            }
        })
    });

    $("#filter3").change(function(){
        let urlSeek = "<a href='<?=BASEURL;?>Search/cblob/"+this.value+"'>Link Json</a>";
        $('#urlSeek').html('');
        $('#urlSeek').html(urlSeek);
        $.ajax({
            dataType:'json',
            url: '<?=BASEURL;?>Search/cblob/'+this.value,
            success: function(resp){
                $("#searchResult tr").remove();
                $.each(resp,function(i,data){
                    $("#searchResult").append(`
                    <tr>
                    <td>${data.companyName}</td>
                    <td>${data.establishedDate}</td>
                    <td>
                        <a href="<?=BASEURL;?>Search/companyInfo/${data.companyId}">
                            Detail
                        </a>
                    </td>
                    </tr>
                    `);
                })
            }
        })
    });
});
</script>