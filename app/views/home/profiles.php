<div class="container">
    
    <div class="row mt-3 pageTitle">
        <div class="col-lg-10">
            <h1>Company Profile</h1>
        </div>
        <div class="col-lg-2 text-center">
            <a class='comNav' href="<?=BASEURL;?>"><i class="fa fa-home"></i> </a>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-12 table-responsie">
            <h4 class="pageSubTitle">General Information</h4>
            <table class="table table-bordered table-sm">
                <tbody>
                    <tr>
                        <th>Company Name</th>
                        <td><?=$data['comGdata']['comp_name'];?></td>
                    </tr>
                    <tr>
                        <th>Line of bussines</th>
                        <td><?=$data['comGdata']['lobName'];?></td>
                    </tr>
                    <tr>
                        <th>Address</th>
                        <td><?=$data['comGdata']['address'];?></td>
                    </tr>
                    <tr>
                        <th>Province</th>
                        <td><?=$data['comGdata']['province'];?></td>
                    </tr>
                    <tr>
                        <th>Country</th>
                        <td><?=$data['comGdata']['country'];?></td>
                    </tr>
                    <tr>
                        <th>Postal Code</th>
                        <td><?=$data['comGdata']['postcode'];?></td>
                    </tr>

                    <tr>
                        <th>Phone</th>
                        <td><?=$data['comGdata']['phone'];?></td>
                    </tr>

                    <tr>
                        <th>Fax</th>
                        <td><?=$data['comGdata']['fax'];?></td>
                    </tr>

                    <tr>
                        <th>Website</th>
                        <td><?=$data['comGdata']['web'];?></td>
                    </tr>

                    <tr>
                        <th>Email</th>
                        <td><?=$data['comGdata']['email'];?></td>
                    </tr>

                    <tr>
                        <th>Detail</th>
                        <td><pre class="ordinary">
                        <?=$data['comGdata']['detail'];?>
                        </pre>
                        </td>
                    </tr>

                </tbody>
            </table>
            <h4 class="pageSubTitle">Aktes</h4>
            <table class="table table-bordered table-sm">
                <!-- aktNomor , aktTanggal , aktNotarisNama , aktNotarisKantor , aktNotarisAlamatBuilding , aktNotarisAlamatStreet -->
                <thead>
                    <tr>
                        <th>Number</th>
                        <th>Date</th>
                        <th>Notary Lawyer</th>
                    </tr>
                </thead>
                <tbody>
                    <?php 
                    
                    foreach($data['comAktes'] AS $akte ): 
                    list($y,$m,$d) = explode("-",$akte['aktTanggal']);
                    ?>
                        <tr>
                            <td><?=$akte['aktNomor'];?></td>
                            <td><?="$m $d, $y";?></td>
                            <td>
                                <?=$akte['aktNotarisNama'];?><br>
                                <?=$akte['aktNotarisKantor'];?>,
                                <?=$akte['aktNotarisBuilding'];?>,
                                <?=$akte['aktNotarisStreet'];?>,
                                <?=$akte['district'];?>,
                                <?=$akte['province'];?>
                                <br>
                            </td>
                        </tr>
                    <?php endforeach; ?>
                </tbody>

            </table>

            <h4 class="pageSubTitle">Branches</h4>
            <table class="table table-bordered table-sm">
                
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>Address</th>
                        <th>Phones</th>
                        <th>Faximiles</th>
                    </tr>
                </thead>
                <tbody>
                    <?php $nu=1; foreach($data['comBranch'] AS $branch): ?>
                        <tr>
                            <td><?=$nu?></td>
                            <td>
                                <?=$branch['branchAddrBuilding'];?><br>
                                <?=$branch['branchAddrStreet'];?>,
                                <?=$branch['city'];?>,
                                <?=$branch['province'];?>
                            </td>
                            <td>
                                <?=$branch['branchPhoneLine1'];?><br>
                                <?=$branch['branchPhoneLine2'];?><br>
                                <?=$branch['branchPhoneLine3'];?><br>
                            </td>
                            <td>
                                <?=$branch['branchFaxLine1'];?><br>
                                <?=$branch['branchFaxLine2'];?><br>
                                <?=$branch['branchFaxLine3'];?><br>
                            </td>
                        </tr>
                    <?php $nu++; endforeach; ?>
                </tbody>
            </table>

            <h4 class="pageSubTitle">Employees</h4>
            <table class="table table-bordered table-sm">
                <thead>
                    <tr>
                        <th>Local</th>
                        <th>Expatriate</th>
                        <th>Total</th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach($data['comEmploy'] AS $empl ): ?>
                        <tr>
                            <td class="text-right"><?=$empl['emplPartLocal'];?></td>
                            <td class="text-right"><?=$empl['emplPartExpat'];?></td>
                            <td class="text-right"><?=$empl['emplPartLocal'] + $empl['emplPartExpat'];?></td>
                        </tr>
                    <?php endforeach;?>
                </tbody>
            </table>

            <h4 class="pageSubTitle">Site Plants</h4>
            
            <table class="table table-bordered table-sm">
                <thead>
                    <tr>
                        <th>Site Name</th>
                        <th>Total Area</th>
                        <th>Total Plant Area</th>
                        <th>Plant Types</th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach($data['compSitePlant'] AS $sp): ?>
                        <tr>
                            <td><?=$sp['spName'];?></td>
                            <td class="text-right"><?=$sp['spAreaTotal'];?> HA</td>
                            <td class="text-right"><?=$sp['spAreaTanamLuas'];?> HA</td>
                            <td><?=$sp['spAreaTanamJenis'];?></td>
                        </tr>
                    <?php endforeach; ?>
                </tbody>
            </table>
        </div>
    </div>

</div>
<?php $this->view('template/bs4js'); ?>
