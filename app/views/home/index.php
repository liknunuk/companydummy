<div class="container">
    
    <div class="row mt-3" id="indexTopNav">
        <div class="col-md-6">
            <p>Please choose province name</p>
        </div>
        <div class="col-md-6 form-horizontal">
            <div class="form-group row">
                <label for="propinsi" class="col-md-4 text-right">Province</label>
                <div class="col-md-8">
                    <select id="prop" class="form-control">
                        <option value="">Choose Province</option>
                        <?php foreach($data['provinces'] AS $province): ?>
                        <option value="<?=$province['areaId'];?>"><?=$province['areaName'];?></option>
                        <?php endforeach; ?>
                    </select>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-12 px-0">
            <table class="table table-bordered table-striped">
                <thead>
                    <tr class="text-center bg-secondary text-light">
                        <th>Company name</th>
                        <th>Address</th>
                        <th>Line Of Bussiness</th>
                        <th>Province</th>
                    </tr>
                </thead>
                <tbody id="comData"></tbody>
            </table>
        </div>
    </div>
</div>

<?php $this->view('template/bs4js');?>
<script>
$('#prop').change( function(){
    $("#comData tr").remove();
    $.ajax({
        dataType:'json',
        url: `<?=BASEURL;?>Search/combypro/${this.value}`,
        success: function(resp){
            $.each( resp , function(i,data){
                $('#comData').append(`
                <tr>
                <!--td>
                    <a href="<?=BASEURL;?>Search/companyInfo/${data.ID}" target="_blank">
                        ${data.comp_name}
                    </a>
                </td-->
                <td>
                    <a href="<?=BASEURL;?>Home/companyProfile/${data.ID}" >
                        ${data.comp_name}
                    </a>
                </td>
                <td>${data.address}</td>
                <td>${data.lobName}</td>
                <td>${data.areaName}</td>
                </tr>
                `);
            })
        }
    })
})
</script>
